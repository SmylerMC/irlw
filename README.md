# IRLW
![IRLW Logo](https://framagit.org/SmylerMC/irlw/raw/master/images/logo.png)

### What is it?
A Minecraft mod adding a new "IRL" world type which uses real world data from [Mapbox](https://www.mapbox.com/)'s API to generate Minecraft worlds. The mod is still in very early development, and has a lot of issues to fix, but is somewhat functional: it is capable of fetching height data and can generate worlds of different sizes. The final goal of the mod is to provide a fully working  world generator, capable of generating various types of worlds, from survival frendly to as realistic as possible.

### Screenshots:
![Example image](https://framagit.org/SmylerMC/irlw/raw/master/images/CustomizeIRLWorld.png)
![Example image](https://framagit.org/SmylerMC/irlw/raw/master/images/Himalayas.png)
 
### Setup:
The mod is still in early development, so except crashes. Internet access is required when playing (unless you want it to crash).

* You will need [Forge](https://files.minecraftforge.net/) for Minecraft 1.12.2.
* Download the mod's jar from framagit [tag section](https://framagit.org/SmylerMC/irlw/tags).
* Add the mod to your mod directory inside your .minecraft directory.
* Create a Mapbox account [here](https://www.mapbox.com/), and generate a token in the account section.
* Launch the modded game, and open the singleplayer menu. You should be prompted to enter the token you just created.
* The token can latter be edited in the mod configuration.
